package com.jackthakar.aomega;

/**
 * Created by jathak on 3/8/14.
 */

public abstract class Datum {
    private double value = 0;
    public int currentUnit = 0;
    public int time = 0;
    public static final int INITIAL = -1;
    public static final int CONSTANT = 0;
    public static final int FINAL = 1;
    public static final int ANY = 2;
    public boolean chemOptional = false;
    public Chemical chemContext = null;
    public boolean hasInfo = false;
    public Formula solvedWith = null;
    public void setValue(double value,Unit unit){
        this.value=unit.toDefault(value);
    }
    public double getValue(Unit unit){
        return unit.fromDefault(value);
    }
    public abstract Unit getDefaultUnit();
    public void setValue(double value){
        setValue(value,getDefaultUnit());
    }
    public double getValue(){
        return getValue(getDefaultUnit());
    }
    public abstract String getName();
    public abstract Unit[] getUnits();
    public Datum time(int time){
        this.time=time;
        return this;
    }
    public Datum chemContext(Chemical c){
        chemContext=c;
        return this;
    }
    public Datum chemOptional(boolean c){
        this.chemOptional=c;
        return this;
    }
    @Override
    public String toString(){
        String additional = "";
        if(hasInfo)additional=":"+solvedWith.getClass().getName();
        if(chemContext!=null)return this.getClass().getName()+":"+value+":"+currentUnit+":"+time+":"+chemContext.name+additional;
        else return this.getClass().getName()+":"+value+":"+currentUnit+":"+time+":null"+additional;
    }
    public static Datum load(String string){
        String[] parts = string.split(":");
        try {
            Datum d = (Datum) Class.forName(parts[0]).newInstance();
            d.setValue(Double.parseDouble(parts[1]));
            d.currentUnit=Integer.parseInt(parts[2]);
            if(parts.length>3){
                d.time=Integer.parseInt(parts[3]);
                if(!parts[4].equals("null"))d.chemContext = new Chemical(parts[4]);
            }
            if(parts.length>5){
                d.solvedWith = (Formula)Class.forName(parts[5]).newInstance();
                d.hasInfo=true;
            }
            return d;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
