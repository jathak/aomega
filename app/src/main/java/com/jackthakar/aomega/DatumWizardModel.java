package com.jackthakar.aomega;

import android.content.Context;

import com.jackthakar.android.wizardpager.model.AbstractWizardModel;
import com.jackthakar.android.wizardpager.model.BranchPage;
import com.jackthakar.android.wizardpager.model.NumberPage;
import com.jackthakar.android.wizardpager.model.Page;
import com.jackthakar.android.wizardpager.model.PageList;
import com.jackthakar.android.wizardpager.model.SingleFixedChoicePage;
import com.jackthakar.aomega.gaslaws.Pressure;
import com.jackthakar.aomega.gaslaws.Temperature;
import com.jackthakar.aomega.gaslaws.VaporizationEnthalpy;
import com.jackthakar.aomega.gaslaws.Volume;
import com.jackthakar.aomega.stoich.Amount;
import com.jackthakar.aomega.stoich.Mass;

import java.util.ArrayList;

/**
 * Created by jathak on 3/8/14.
 */
public class DatumWizardModel extends AbstractWizardModel {
    private Context ctx;
    public DatumWizardModel(Context ctx){
        super(ctx);
    }


    @Override
    protected PageList onNewRootPageList() {
        ctx=mContext;
        Page type = new BranchPage(this, "Data Type")
                .addBranch("Pressure",
                        getBranch(new Pressure().getUnits()),chemContext(1))
                .addBranch("Temperature",
                        getBranch(new Temperature().getUnits()))
                .addBranch("Volume",
                        getBranch(new Volume().getUnits()))
                .addBranch("Amount",
                        getBranch(new Amount().getUnits()),chemContext(0))
                .addBranch("Mass",
                        getBranch(new Mass().getUnits()),chemContext(0))
                .addBranch("Enthalpy of Vaporization",
                        getBranch(new VaporizationEnthalpy().getUnits()))
                        .setRequired(true);
        PageList pl = new PageList(type,new NumberPage(this,"Value"),
                new SingleFixedChoicePage(this,"Initial or Final?")
        .setChoices("Initial","Final","Does not change"));
        return pl;
    }
    private Page getBranch(Unit[] units){
        String[] uNames = new String[units.length];
        for(int i=0;i<uNames.length;i++){
            uNames[i]=units[i].getName();
        }
        return new SingleFixedChoicePage(this,"Units").setChoices(uNames).setRequired(true);
    }
    private Page chemContext(int type){
        if(MainActivity.equation==null)return null;
        ArrayList<String> pages = new ArrayList<String>();
        pages.add("None");
        if(type==0){
            for(Chemical c:MainActivity.equation.reactants){
                pages.add(MainActivity.subize(c.name));
            }
            for(Chemical c:MainActivity.equation.products){
                pages.add(MainActivity.subize(c.name));
            }
        }else if(type==1){
            for(Chemical c:MainActivity.equation.reactants){
                if(c.state==Chemical.GAS)pages.add(MainActivity.subize(c.name));
            }
            for(Chemical c:MainActivity.equation.products){
                if(c.state==Chemical.GAS)pages.add(MainActivity.subize(c.name));
            }
        }
        return new SingleFixedChoicePage(this,"Chemical").setChoices(pages.toArray(new String[]{})).setRequired(true);
    }
}