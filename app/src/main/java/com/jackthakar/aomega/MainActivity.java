package com.jackthakar.aomega;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.jackthakar.aomega.molarmass.MolarMass;
import com.jackthakar.aomega.stoich.StoichFormula;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import de.timroes.android.listview.EnhancedListView;


public class MainActivity extends Activity {
    public static List<Datum> data = new ArrayList<Datum>();
    public static ChemicalEquation equation;
    private Datum lastDatum;
    private DataAdapter adapter;
    private int lastIndex;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences prefs = getSharedPreferences("problem",0);
        String saveData = prefs.getString("saveData",null);
        String eqData = prefs.getString("eqData",null);
        MolarMass.init(this);
        if(eqData!=null){
            equation = ChemicalEquation.load(eqData);
        }
        data.clear();
        if(saveData!=null){
            String[] parts = saveData.split(";");
            for(String p:parts){
                Datum d = Datum.load(p);
                if(d!=null)data.add(d);
            }
        }
        EnhancedListView list = (EnhancedListView) findViewById(R.id.list);
        list.setDividerHeight(0);
        adapter = new DataAdapter(this);
        list.setAdapter(adapter);
        list.setDismissCallback(new EnhancedListView.OnDismissCallback() {
            @Override
            public EnhancedListView.Undoable onDismiss(EnhancedListView lv, final int i) {
                lastDatum = data.remove(i);
                lastIndex = i;
                adapter.notifyDataSetChanged();
                save();
                return null;
                /*return new EnhancedListView.Undoable() {
                    @Override
                    public void undo() {
                        data.add(i, lastDatum);
                        adapter.notifyDataSetChanged();
                    }
                };*/
            }
        });
        list.setUndoHideDelay(1000);
        list.enableSwipeToDismiss();
        Button add = (Button)findViewById(R.id.add_data);
        Button solve = (Button)findViewById(R.id.solve_for);
        TextView eqtext = (TextView)findViewById(R.id.equation);
        eqtext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(MainActivity.this, EquationActivity.class), 3);
            }
        });
        if(equation!=null)eqtext.setText(equation.getFormattedString());
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WizardActivity.newDatum = null;
                startActivityForResult(new Intent(MainActivity.this, WizardActivity.class), 2);
            }
        });
        solve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                while(runFormulas());
            }
        });
    }

    public boolean runFormulas(){
        List<Formula> forms = new ArrayList<Formula>();
        for (Formula f : Formula.getFormulas()) {
            f.reset();
            if (f instanceof StoichFormula) {
                ((StoichFormula) f).equation = ChemicalEquation.load(equation.getStorageString());
            }
            f.getComponentList();
            int chosenAny = -2;
            int counter = 0;
            for (int i = 0; i < data.size(); i++) {
                Datum datum = data.get(i);
                for (int j = 0; j < f.getComponentList().length; j++) {
                    //System.out.println(f.getComponentList()[j].getClass().getName()+";"+datum.getClass().getName());
                    if (f.variableList[j] == null && f.getComponentList()[j].getName().equals(datum.getName())) {
                        System.out.println(f.getClass().getCanonicalName() + ": 1 : " + datum.getClass().getName());
                        if (f.getComponentList()[j].chemOptional||(f.getComponentList()[j].chemContext == null && datum.chemContext == null)
                                || ((f.getComponentList()[j].chemContext != null && datum.chemContext != null)
                                && datum.chemContext.name.equals(f.getComponentList()[j].chemContext.name))) {
                            System.out.println(f.getClass().getCanonicalName() + ": 2 : " + datum.getClass().getName());
                            if ((f.getComponentList()[j].time == Datum.ANY && (chosenAny == -2 || chosenAny == datum.time)) || f.getComponentList()[j].time == datum.time) {
                                System.out.println(f.getClass().getCanonicalName() + ": 3 : " + datum.getClass().getName());
                                f.setVariable(j, datum);
                                if (f.getComponentList()[j].time == Datum.ANY)chosenAny = datum.time;
                                counter++;
                                break;
                            }
                        }
                    }
                }
            }
            Datum d = null;
            if (counter >= f.minVariables() && counter <= f.maxVariables()){
                d = f.solveFor(0);
            }
            else System.out.println("Found " + counter + " variables for " + f.getClass().getCanonicalName());

            List<Datum> tobeadded = new ArrayList<Datum>();
            if (d != null) {
                int count = 0;
                for(Datum v:f.variableList){
                    if(v!=null) {
                        boolean found = false;
                        for (Datum c : data) {
                            if (v.getName().equals(c.getName()) && (v.time == c.time) && ((v.chemContext==null&&c.chemContext==null)||(v.chemContext!=null&&c.chemContext!=null&&v.chemContext.name.equals(c.chemContext.name)))) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            tobeadded.add(v);
                            v.hasInfo=true;
                            v.solvedWith=f;
                            count++;
                        }
                    }
                }
                for(Datum v:tobeadded)data.add(v);
                if(count>0) {
                    adapter.notifyDataSetChanged();
                    save();
                    return true;
                }
            } else System.out.println(f.getClass().getCanonicalName() + " returned null");
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem m) {
        if(m.getItemId()==R.id.clearAll){
            data.clear();
            equation=null;
            ((TextView)findViewById(R.id.equation)).setText("Tap to add equation");
            save();
            adapter.notifyDataSetChanged();
        }else if(m.getItemId()==R.id.clearSolved){
            for(int i=0;i<data.size();i++){
                if(data.get(i).solvedWith!=null){
                    data.remove(i);
                    i--;
                }
            }
            save();
            adapter.notifyDataSetChanged();
        }

        return false;
    }

    @Override
    public void onPause(){
        super.onPause();
        if(data.size()==0)return;
        save();
    }

    private void save() {
        SharedPreferences prefs = getSharedPreferences("problem",0);
        SharedPreferences.Editor e = prefs.edit();
        String setting = "";
        for(Datum d:data){
            setting+=";"+d.toString();
        }
        if(setting.length()>0)setting.substring(1);
        e.putString("saveData",setting);
        if(equation!=null){
            String settingEq = equation.getStorageString();
            e.putString("eqData",settingEq);
        }else e.putString("eqData",null);
        e.commit();
    }

    @Override
    public void onActivityResult(int requestCode,int resultCode,Intent i){
        if(requestCode==2){
            if(WizardActivity.newDatum!=null){
                data.add(WizardActivity.newDatum);
                adapter.notifyDataSetChanged();
                save();
            }
        }if(requestCode==3){
            ((TextView)findViewById(R.id.equation)).setText(equation.getFormattedString());
        }
    }

    public static class DataAdapter extends BaseAdapter{
        private Context ctx;
        public DataAdapter(Context ctx){
            this.ctx = ctx;
        }
        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int i) {
            return data.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            View v = convertView;
            final Datum datum = data.get(i);
            //System.out.println(scene.speeches.size()+";"+speech.lines.size());
            if(v==null){
                v=((LayoutInflater)ctx.getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.datum, null);
            }
            TextView type = (TextView) v.findViewById(R.id.type);
            ImageView info = (ImageView) v.findViewById(R.id.info);
            if(datum.hasInfo){
                info.setVisibility(View.VISIBLE);
                info.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FormulaActivity.loadFormula=datum.solvedWith;
                        ctx.startActivity(new Intent(ctx,FormulaActivity.class));
                    }
                });
            }else info.setVisibility(View.GONE);

            String typeLabel = datum.getName().toUpperCase();
            if(datum.time==-1)typeLabel="INITIAL "+typeLabel;
            if(datum.time==1) typeLabel="FINAL "+typeLabel;
            if(datum.chemContext!=null)typeLabel+=" OF "+datum.chemContext.name;
            typeLabel = subize(typeLabel);
            type.setText(Html.fromHtml(typeLabel));
            final TextView value = (TextView) v.findViewById(R.id.value);
            Spinner units = (Spinner) v.findViewById(R.id.units);
            String[] unitArray = new String[datum.getUnits().length];
            for(int j=0;j<unitArray.length;j++){
                unitArray[j]=datum.getUnits()[j].getShortName();
            }
            units.setAdapter(new ArrayAdapter<String>(ctx, R.layout.spinner_row, unitArray));
            units.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    datum.currentUnit=i;
                    double d = datum.getValue(datum.getUnits()[i]);
                    String num = "" + round(d, 4);
                    if(num.contains("E")) {
                        num = num.replaceAll("E", " x 10<small><small><small><sup><b>");
                        num += "</b></sup></small></small></small>";
                    }
                    value.setText(Html.fromHtml(num));
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            units.setSelection(datum.currentUnit);
            return v;
        }
        public static double round(double value, int places) {
            if (places < 0) throw new IllegalArgumentException();

            BigDecimal bd = new BigDecimal(value);
            bd = bd.setScale(places, RoundingMode.HALF_UP);
            return bd.doubleValue();
        }
    }
    private static final String S = "<sub><small><small><small><b>";
    private static final String E = "</b></small></small></small></sub>";
    public static String subize(String s){
        s=s.replaceAll("0",S+"0"+E);
        s=s.replaceAll("1",S+"1"+E);
        s=s.replaceAll("2",S+"2"+E);
        s=s.replaceAll("3",S+"3"+E);
        s=s.replaceAll("4",S+"4"+E);
        s=s.replaceAll("5",S+"5"+E);
        s=s.replaceAll("6",S+"6"+E);
        s=s.replaceAll("7",S+"7"+E);
        s=s.replaceAll("8",S+"8"+E);
        s=s.replaceAll("9",S+"9"+E);
        return s;
    }
    public static String unsubize(String s){
        s=s.replaceAll(S+"0"+E,"0");
        s=s.replaceAll(S+"1"+E,"1");
        s=s.replaceAll(S+"2"+E,"2");
        s=s.replaceAll(S+"3"+E,"3");
        s=s.replaceAll(S+"4"+E,"4");
        s=s.replaceAll(S+"5"+E,"5");
        s=s.replaceAll(S+"6"+E,"6");
        s=s.replaceAll(S+"7"+E,"7");
        s=s.replaceAll(S+"8"+E,"8");
        s=s.replaceAll(S+"9"+E,"9");
        return s;
    }
    /*s=s.replaceAll("₀","0");
        s=s.replaceAll("₁","1");
        s=s.replaceAll("₂","2");
        s=s.replaceAll("₃","3");
        s=s.replaceAll("₄","4");
        s=s.replaceAll("₅","5");
        s=s.replaceAll("₆","6");
        s=s.replaceAll("₇","7");
        s=s.replaceAll("₈","8");
        s=s.replaceAll("₉","9");*/
}
