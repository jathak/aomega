package com.jackthakar.aomega;

import com.jackthakar.aomega.molarmass.MolarMass;

/**
 * Created by davidlee on 3/8/14.
 */
public class Chemical {
    public String name;
    public int state;
    public static final int GAS = 0;
    public static final int LIQUID = 1;
    public static final int SOLID = 2;
    public int coefficient;
    public Chemical(String n){
        name = n;
    }

    public MolarMass getMolarMass(){
        return MolarMass.getMass(name);
    }

    public void setName(String n){
        name = n;
    }

    @Override
    public String toString(){
        return name;
    }

}
