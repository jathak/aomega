package com.jackthakar.aomega;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;

import de.timroes.android.listview.EnhancedListView;


public class EquationActivity extends Activity {
    public BaseAdapter adapter;
    public ChemicalEquation equation;
    private Chemical lastChemical;
    private static TextView eqtext;
    private int lastIndex;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equation);
        EnhancedListView list = (EnhancedListView) findViewById(R.id.list);
        if(MainActivity.equation==null)MainActivity.equation=new ChemicalEquation();
        equation = MainActivity.equation;
        adapter = new EquationAdapter(equation,this);
        list.setAdapter(adapter);
        list.setDismissCallback(new EnhancedListView.OnDismissCallback() {
            @Override
            public EnhancedListView.Undoable onDismiss(EnhancedListView lv, final int i) {
                if(i<equation.reactants.size())lastChemical = equation.reactants.remove(i);
                else lastChemical = equation.products.remove(i-equation.reactants.size());
                lastIndex = i;
                adapter.notifyDataSetChanged();
                if(eqtext!=null)eqtext.setText(equation.getFormattedString());
                return null;
                /*return new EnhancedListView.Undoable() {
                    @Override
                    public void undo() {
                        if(i<equation.reactants.size())equation.reactants.add(i, lastChemical);
                        else equation.products.add(i - equation.reactants.size(), lastChemical);
                        adapter.notifyDataSetChanged();
                        if(eqtext!=null)eqtext.setText(equation.getFormattedString());
                    }
                };*/
            }
        });
        list.setUndoHideDelay(1000);
        list.enableSwipeToDismiss();
        list.setDividerHeight(0);
        Button add = (Button)findViewById(R.id.add_data);
        Button solve = (Button)findViewById(R.id.solve_for);
        eqtext = (TextView)findViewById(R.id.equation);
        if(equation!=null)eqtext.setText(equation.getFormattedString());
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EquationActivity.this);
                builder.setTitle("Chemical Formula");
                final EditText input = new EditText(EquationActivity.this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        equation.addReactant(new Chemical(input.getText().toString()));
                        adapter.notifyDataSetChanged();
                        if(eqtext!=null)eqtext.setText(equation.getFormattedString());
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                adapter.notifyDataSetChanged();
            }
        });
        solve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EquationActivity.this);
                builder.setTitle("Chemical Formula");
                final EditText input = new EditText(EquationActivity.this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        equation.addProduct(new Chemical(input.getText().toString()));
                        adapter.notifyDataSetChanged();
                        if(eqtext!=null)eqtext.setText(equation.getFormattedString());
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                adapter.notifyDataSetChanged();
            }
        });
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        eqtext=null;
    }
    public static class EquationAdapter extends BaseAdapter{
        private Context ctx;
        private ChemicalEquation equation;
        public EquationAdapter(ChemicalEquation equation,Context ctx){
            this.ctx = ctx;
            this.equation=equation;
        }
        @Override
        public int getCount() {
            return equation.reactants.size()+equation.products.size();
        }

        @Override
        public Object getItem(int i) {
            if(i<equation.reactants.size())return equation.reactants.get(i);
            else return equation.products.get(i-equation.reactants.size());
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            View v = convertView;
            Chemical c;
            boolean isReactant;
            if(i<equation.reactants.size()){
                c=equation.reactants.get(i);
                isReactant=true;
            }
            else{
                c=equation.products.get(i-equation.reactants.size());
                isReactant=false;
            }
            final Chemical chemical = c;
            //System.out.println(scene.speeches.size()+";"+speech.lines.size());
            if(v==null){
                v=((LayoutInflater)ctx.getSystemService
                        (Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.chemical, null);
            }
            TextView type = (TextView) v.findViewById(R.id.type);
            if(isReactant)type.setText("Reactant");
            else type.setText("Product");
            final TextView value = (TextView) v.findViewById(R.id.value);
            value.setText(Html.fromHtml(MainActivity.subize(chemical.name)));
            value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                    builder.setTitle("Chemical Formula");
                    final EditText input = new EditText(ctx);
                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                    input.setText(chemical.name);
                    builder.setView(input);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            chemical.name = input.getText().toString();
                            notifyDataSetChanged();
                            if(eqtext!=null)eqtext.setText(equation.getFormattedString());
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();
                }
            });
            Spinner units = (Spinner) v.findViewById(R.id.units);
            Integer[] unitArray = new Integer[50];
            for(int j=0;j<50;j++){
                unitArray[j] = j+1;
            }
            units.setAdapter(new ArrayAdapter<Integer>(ctx, R.layout.spinner_row, unitArray));
            units.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    chemical.coefficient=i+1;
                    if(eqtext!=null)eqtext.setText(equation.getFormattedString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            units.setSelection(chemical.coefficient-1);
            final RadioButton solid = (RadioButton)v.findViewById(R.id.solid);
            final RadioButton liquid = (RadioButton)v.findViewById(R.id.liquid);
            final RadioButton gas = (RadioButton)v.findViewById(R.id.gas);
            if(chemical.state==Chemical.SOLID){
                solid.setChecked(true);
                liquid.setChecked(false);
                gas.setChecked(false);
            }else if(chemical.state==Chemical.LIQUID){
                solid.setChecked(false);
                liquid.setChecked(true);
                gas.setChecked(false);
            }else if(chemical.state==Chemical.GAS){
                solid.setChecked(false);
                liquid.setChecked(false);
                gas.setChecked(true);
            }
            solid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if(b)chemical.state=Chemical.SOLID;
                    if(eqtext!=null)eqtext.setText(equation.getFormattedString());
                }
            });
            liquid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) chemical.state = Chemical.LIQUID;
                    if (eqtext != null) eqtext.setText(equation.getFormattedString());
                }
            });
            gas.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) chemical.state = Chemical.GAS;
                    if (eqtext != null) eqtext.setText(equation.getFormattedString());
                }
            });
            return v;
        }
        public static double round(double value, int places) {
            if (places < 0) throw new IllegalArgumentException();

            BigDecimal bd = new BigDecimal(value);
            bd = bd.setScale(places, RoundingMode.HALF_UP);
            return bd.doubleValue();
        }
    }
}
