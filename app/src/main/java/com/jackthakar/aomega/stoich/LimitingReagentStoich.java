package com.jackthakar.aomega.stoich;

import com.jackthakar.aomega.Datum;
import com.jackthakar.aomega.Unit;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Shreyas on 3/9/14.
 */

public class LimitingReagentStoich extends ReactionFormula {
    private Unit grams = new Mass.Grams();
    private int count;
    Datum LimitingReagent= new Amount();
    @Override
    public int minVariables() {
        return 1;
    }

    @Override
    public int maxVariables() {
        return 4;
    }
    @Override
    public Datum solveFor(int index) {
        List<Integer> dontloop = new ArrayList<Integer>();
        for(int i = 0; i < variableList.length; i++){
            if(variableList[i]!=null && variableList[i].getName().equals("Mass")&&!dontloop.contains(i)&&variableList[i].time==-1){
               variableList[i-variableList.length/2]= new Amount().chemContext(getComponentList()[i-variableList.length/2].chemContext).time(-1);
               variableList[i-variableList.length/2].setValue(variableList[i].getValue(grams)*1/(variableList[i].chemContext.getMolarMass().getValue()));
            }else if(variableList[i]!=null && variableList[i].getName().equals("Amount")&&variableList[i].time==-1){
                variableList[i+variableList.length/2]= new Mass().chemContext(getComponentList()[i+variableList.length/2].chemContext).time(-1);
                variableList[i+variableList.length/2].setValue(variableList[i].getValue()*(variableList[i].chemContext.getMolarMass().getValue()));
                dontloop.add(i+variableList.length/2);
            }
        }
        int count = 0;
        for(Datum v:variableList){
            if(v!=null)count++;
        }
        if(count==2)return super.solveFor(index);
        if(count!=4)return null;
        double ratio = (double) getCoefficient(getComponentList()[1].chemContext) / (double) getCoefficient(getComponentList()[0].chemContext);
        double value= variableList[0].getValue() * ratio;
        if(value<variableList[1].getValue()){
            count= 0;
            LimitingReagent= variableList[0];
            System.out.println(LimitingReagent.getValue());
            //variableList[1]= new Amount().chemContext(getComponentList()[1].chemContext);
            //variableList[1 + variableList.length/2] = new Mass().chemContext(getComponentList()[1+variableList.length/2].chemContext);
        }
        else{
            count=1;
            LimitingReagent= variableList[1];
            System.out.println(LimitingReagent.getValue());
            //variableList[0] = new Amount().chemContext(getComponentList()[0].chemContext);
            //variableList[variableList.length/2] = new Mass().chemContext(getComponentList()[variableList.length/2].chemContext);
        }

        for(int i=0; i<variableList.length; i++){

                solverFunction(getComponentList()[i], getComponentList()[count], i);

        }

        return variableList[index];


}
    @Override
    public void solverFunction(Datum unknown, Datum known, int i) {
        double ratio = (double)getCoefficient(unknown.chemContext) / (double)getCoefficient(known.chemContext);
        System.out.println(ratio);
        if(known.getName().equals("Amount")){
            if(unknown.getName().equals("Amount")){
                if(variableList[i]!=null&&variableList[i]!=LimitingReagent){
                    double oldValue = variableList[i].getValue();
                    variableList[i] = new Amount().chemContext(getComponentList()[i].chemContext).time(-1*getComponentList()[i].time);
                    variableList[i].setValue(oldValue-(LimitingReagent.getValue() * ratio));
                }else{
                    variableList[i] = new Amount().chemContext(getComponentList()[i].chemContext).time(getComponentList()[i].time);
                    variableList[i].setValue(LimitingReagent.getValue() * ratio);
                }
            }
            else{
                if(variableList[i]!=null&&variableList[i]!=LimitingReagent){
                    double oldValue = variableList[i].getValue(grams);
                    variableList[i] = new Mass().chemContext(getComponentList()[i].chemContext).time(-1*getComponentList()[i].time);;
                    variableList[i].setValue(oldValue-(LimitingReagent.getValue() * ratio * unknown.chemContext.getMolarMass().getValue()),grams);
                }else{
                    variableList[i] = new Mass().chemContext(getComponentList()[i].chemContext).time(getComponentList()[i].time);;
                    variableList[i].setValue(LimitingReagent.getValue() * ratio * unknown.chemContext.getMolarMass().getValue(),grams);
                }
            }
        }
        if(variableList[i].getValue()==0)variableList[i]=null;
    }
}
