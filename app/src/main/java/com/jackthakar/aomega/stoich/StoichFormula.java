package com.jackthakar.aomega.stoich;

import com.jackthakar.aomega.ChemicalEquation;
import com.jackthakar.aomega.Datum;
import com.jackthakar.aomega.Formula;

/**
 * Created by Shreyas on 3/8/14.
 */
public abstract class StoichFormula extends Formula {
public ChemicalEquation equation;

    @Override
    public abstract Datum[] getComponentList();

    @Override
    public abstract Datum solveFor(int index);


}
