package com.jackthakar.aomega.stoich;

import com.jackthakar.aomega.Datum;
import com.jackthakar.aomega.Unit;

/**
 * Created by jathak on 3/8/14.
 */
public class Mass extends Datum {
    @Override
    public Unit getDefaultUnit() {
        return new Kilograms();
    }

    @Override
    public String getName() {
        return "Mass";
    }

    @Override
    public Unit[] getUnits() { return new Unit[]{new Kilograms(), new Grams(), new Milligrams(), new Pounds()};}

    public static class Kilograms extends Unit {
        @Override
        public String getName() {
            return "kilograms";
        }

        @Override
        public String getShortName() {
            return "kg";
        }

        @Override
        public double toDefault(double inUnits) {
            return inUnits;
        }

        @Override
        public double fromDefault(double inDefault) {
            return inDefault;
        }
    }
    public static class Grams extends Unit {
        @Override
        public String getName() {
            return "grams";
        }

        @Override
        public String getShortName() {
            return "g";
        }

        @Override
        public double toDefault(double inUnits) {
            return inUnits/1000;
        }

        @Override
        public double fromDefault(double inDefault) {
            return inDefault*1000;
        }
    }
    public static class Milligrams extends Unit {
        @Override
        public String getName() {
            return "milligrams";
        }

        @Override
        public String getShortName() {
            return "mg";
        }

        @Override
        public double toDefault(double inUnits) {
            return inUnits/1000000;
        }

        @Override
        public double fromDefault(double inDefault) {
            return inDefault*1000000;
        }
    }
    public static class Pounds extends Unit {
        @Override
        public String getName() {
            return "Pounds (on earth)";
        }

        @Override
        public String getShortName() {
            return "lbs";
        }

        @Override
        public double toDefault(double inUnits) {
            return inUnits*2.204622622;
        }

        @Override
        public double fromDefault(double inDefault) {
            return inDefault/2.204622622;
        }
    }
}
