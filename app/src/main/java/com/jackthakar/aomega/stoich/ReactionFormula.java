package com.jackthakar.aomega.stoich;

import android.app.Fragment;

import com.jackthakar.aomega.Chemical;
import com.jackthakar.aomega.Datum;
import com.jackthakar.aomega.Unit;

/**
 * Created by Shreyas on 3/8/14.
 */
public class ReactionFormula extends StoichFormula {
    private Unit grams = new Mass.Grams();
    @Override
    public int minVariables() {
        return 1;
    }

    @Override
    public int maxVariables() {
        return 1;
    }

    @Override
    public Fragment getLayout() {
        return new BasicStoichiometryFragment();
    }

    @Override
    public String getFormulaName() {
        return "Basic Stoichiometry";
    }

    @Override
    public Datum[] getComponentList() {
        if(variableList==null) {
            variableList = new Datum[2 * (equation.reactants.size() + equation.products.size())];
        }
        //Datum array that is being instantiated with a specific size number based on reactants & products
        Datum[] componentList = new Datum[2 * (equation.reactants.size() + equation.products.size())];
        //populating componentList with new amount
        // objects for the first half of the initial array
        //preparing componentList
        for (int i = 0; i < componentList.length; i++) {
            if (i < componentList.length / 2) {
                int k = i;
                //Moles
                ///if k is less than equation reactants.size it will  add empty Amount object
                componentList[i] = new Amount();
                //if k index still refers to reactants, then it will set the componentList variable chemContext to the chemical
                //equation in reactants
                if (k < equation.reactants.size()) {
                    componentList[i].chemContext = equation.reactants.get(k);
                    componentList[i].time(-1);
                }else {
                    componentList[i].chemContext = equation.products.get(k - equation.reactants.size());
                    componentList[i].time(1);
                }
            } else {
                int k = i - componentList.length / 2;
                //Grams
                componentList[i] = new Mass();
                if (k < equation.reactants.size()) {
                    componentList[i].chemContext = equation.reactants.get(k);
                    componentList[i].time(-1);
                }else {
                    componentList[i].chemContext = equation.products.get(k - equation.reactants.size());
                    componentList[i].time(1);
                }
            }

        }
        return componentList;
    }

    //NOW, variableList is populated with known values
    @Override

    public Datum solveFor(int index) {
        int count = 0;
        for (int j = 0; j < variableList.length; j++) {
            if (variableList[j] != null) {
                count = j;
                break;
            }

        }
        Datum[] componentList = getComponentList();
        for (int i = 0; i < variableList.length; i++) {
//            if (variableList[i] != null) {
//                if (variableList[i].getName().equals("Amount") && variableList[variableList.length / 2 + i] == null) {
//                    variableList[variableList.length / 2 + i] = new Mass();
//                    variableList[variableList.length / 2 + i].setValue(variableList[i].getValue() * variableList[i].chemContext.getMolarMass().getValue(), grams);
//                }
//                else if (variableList[i].getName().equals("Mass") && variableList[i-variableList.length /2 ] == null) {
//                    variableList[i-variableList.length / 2] = new Amount();
//                    variableList[i-variableList.length / 2].setValue(variableList[i].getValue(grams) * 1 / (variableList[i].chemContext.getMolarMass().getValue()));
//                }
////            }
//            else {
            if(i!=count){
                solverFunction(componentList[i], variableList[count], i);
            }
//            }
//        }
            checkContext();

        }
        return variableList[index];
    }
    private void checkContext() {
        for(int i=0;i<variableList.length;i++){
            Datum v = variableList[i];
            if(v!=null){
                Datum c = getComponentList()[i];
                v.chemContext=c.chemContext;
                v.time(c.time);
            }
        }
    }


//
//            if(data.getName().equals(componentList[i].getName()) && componentList[i].chemContext.name.equals(data.chemContext.name)){
//                variableList[i]= data;
//                if(data.getName().equals("Amount")){
//                   //We know we have an Amount now we need a Mass
//                  variableList[variableList.length/2 + i] = new Mass();
//                  variableList[i].setValue(variableList[i].getValue()* variableList[i].chemContext.getMolarMass().getValue());
//                }
//                if(data.getName().equals("Mass")){
//                    //We know we have an Mass, now we need a Amount
//                    variableList[variableList.length/2 - i] = new Amount();
//                    variableList[i].setValue(variableList[i].getValue()* 1/(variableList[i].chemContext.getMolarMass().getValue()));
//                }
//                break;
//            }
//            else{
//                solverFunction(componentList[i], data);
//            }

//        }


    public void solverFunction(Datum unknown, Datum known, int i) {
        double ratio = (double)getCoefficient(unknown.chemContext) / (double)getCoefficient(known.chemContext);
        if(known.getName().equals("Amount")){
            if(unknown.getName().equals("Amount")){
                variableList[i] = new Amount();
                variableList[i].setValue(known.getValue() * ratio);
            }
            else{
                variableList[i] = new Mass();
                variableList[i].setValue(known.getValue() * ratio * unknown.chemContext.getMolarMass().getValue(),grams);
            }
        }

        else if (known.getName().equals("Mass")) {
            if (unknown.getName().equals("Amount")) {
                variableList[i] = new Amount();
                variableList[i].setValue(known.getValue(grams) * 1/known.chemContext.getMolarMass().getValue()*ratio);
            } else {
                variableList[i] = new Mass();
                variableList[i].setValue(known.getValue(grams) * 1/known.chemContext.getMolarMass().getValue()*ratio*unknown.chemContext.getMolarMass().getValue(),grams);
            }
        }
    }

    public int getCoefficient(Chemical c){
        for(Chemical r:equation.reactants){
            if(r.name.equals(c.name))return r.coefficient;
        }
        for(Chemical p:equation.products){
            if(p.name.equals(c.name))return p.coefficient;
        }
        return 1;
    }
}
