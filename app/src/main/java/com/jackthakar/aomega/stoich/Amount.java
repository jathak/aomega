package com.jackthakar.aomega.stoich;

import com.jackthakar.aomega.Datum;
import com.jackthakar.aomega.Unit;

/**
 * Created by jathak on 3/8/14.
 */
public class Amount extends Datum{
    public static final double AVOGADRO = 6.02214129;
    @Override
    public Unit getDefaultUnit() {
        return new Moles();
    }

    @Override
    public String getName() {
        return "Amount";
    }

    @Override
    public Unit[] getUnits() { return new Unit[]{new Moles(), new Particles(),new Atoms(),new Molecules()};}

    public static class Moles extends Unit{

        @Override
        public String getName() {
            return "moles";
        }

        @Override
        public String getShortName() {
            return "mol";
        }

        @Override
        public double toDefault(double inUnits) {
            return inUnits;
        }

        @Override
        public double fromDefault(double inDefault) {
            return inDefault;
        }
    }
    public static class Particles extends Unit{

        @Override
        public String getName() {
            return "x10^23 particles";
        }

        @Override
        public String getShortName() {
            return getName();
        }

        @Override
        public double toDefault(double inUnits) {
            return inUnits/AVOGADRO;
        }

        @Override
        public double fromDefault(double inDefault) {
            return inDefault*AVOGADRO;
        }
    }
    public static class Atoms extends Particles{
        @Override
        public String getName() {
            return "x10^23 atoms";
        }
    }
    public static class Molecules extends Particles{
        @Override
        public String getName() {
            return "x10^23 molecules";
        }
    }
}
