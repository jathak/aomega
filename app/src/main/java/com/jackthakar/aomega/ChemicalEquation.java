package com.jackthakar.aomega;

import android.text.Html;
import android.text.Spanned;

import java.util.ArrayList;

/**
 * Created by davidlee on 3/8/14.
 */
public class ChemicalEquation {
    public ChemicalEquation(){
        reactants = new ArrayList<Chemical>();
        products = new ArrayList<Chemical>();
    }
    public ArrayList<Chemical> reactants;
    public ArrayList<Chemical> products;
    public void addReactant(Chemical... c){
        for(Chemical c1:c)reactants.add(c1);
    }

    public ArrayList<Chemical> getReactants(){
        return reactants;
    }

    public void addProduct(Chemical... c){
        for(Chemical c1:c)products.add(c1);
    }

    public ArrayList<Chemical> getProducts(){
        return products;
    }

    public Spanned getFormattedString(){
        String rString = "";
        for(Chemical c:reactants){
            rString+="+ ";
            if(c.coefficient>1)rString+=c.coefficient+" ";
            rString+=MainActivity.subize(c.name)+" ";
        }
        if(reactants.size()>0)rString = rString.substring(2);
        String pString = "";
        for(Chemical c:products){
            pString+=" + ";
            if(c.coefficient>1)pString+=c.coefficient+" ";
            pString+=MainActivity.subize(c.name);
        }
        if(products.size()>0)pString = pString.substring(2);
        return Html.fromHtml(rString+"→"+pString);
    }
    public String getStorageString(){
        String rString = "";
        for(Chemical c:reactants){
            rString+=";";
            rString+=c.coefficient+":";
            rString+=c.name+":";
            rString+=c.state;
        }
        rString = rString.substring(1);
        String pString = "";
        for(Chemical c:products){
            pString+=";";
            pString+=c.coefficient+":";
            pString+=c.name+":";
            pString+=c.state;
        }
        pString = pString.substring(1);
        return rString+","+pString;
    }
    public static ChemicalEquation load(String storage){
        String[] halves = storage.split(",");
        ChemicalEquation eq = new ChemicalEquation();
        for(String s:halves[0].split(";")){
            String[] parts = s.split(":");
            Chemical c = new Chemical(parts[1]);
            c.coefficient = Integer.parseInt(parts[0]);
            c.state = Integer.parseInt(parts[2]);
            eq.addReactant(c);
        }
        for(String s:halves[1].split(";")){
            String[] parts = s.split(":");
            Chemical c = new Chemical(parts[1]);
            c.coefficient = Integer.parseInt(parts[0]);
            c.state = Integer.parseInt(parts[2]);
            eq.addProduct(c);
        }
        return eq;
    }
}
