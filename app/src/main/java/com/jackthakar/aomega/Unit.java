package com.jackthakar.aomega;

/**
 * Created by jathak on 3/8/14.
 */
public abstract class Unit {
    public abstract String getName();
    public abstract String getShortName();
    public abstract double toDefault(double inUnits);
    public abstract double fromDefault(double inDefault);
}
