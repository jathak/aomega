package com.jackthakar.aomega.molarmass;
import android.content.Context;

import com.jackthakar.aomega.Datum;
import com.jackthakar.aomega.Unit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Shreyas & Ethan on 3/8/14
 */
//Class to return Molar Mass numerical double
public class MolarMass extends Datum{
    //initial string input cut into a character array
    private static char[] cutSubstance;
    //uses cutSubstance and converts input into ArrayList of elements, parenthesis & numbers
    private static ArrayList elements = new ArrayList();
    //reference for elements & their molar masses
    private static HashMap<String, Double> masses = new HashMap<String,Double>();
    //holder array for HashMap
//    private static ArrayList<String> compare = new ArrayList<String>();
    //BufferedReader buf= new BufferedReader(new FileReader("elements.csv"));

    public static MolarMass getMass(String substance) {
        System.out.println("Substance: "+substance);
        cutSubstance=null;
        cutSubstance = substance.toCharArray();
        elements=new ArrayList();
        System.out.println(elements);
        for (int i = 0; i < cutSubstance.length; i++) {
            if (Character.isUpperCase(cutSubstance[i]))
                elements.add(Character.toString(cutSubstance[i]));
            else if (Character.isLowerCase(cutSubstance[i]))
                elements.set(elements.size() - 1, Character.toString(cutSubstance[i - 1]) + Character.toString(cutSubstance[i]));
            else if (Character.isDigit(cutSubstance[i]))
                elements.add(Character.getNumericValue(cutSubstance[i]));
            else
                elements.add(cutSubstance[i]);
            System.out.println(elements);
        }
        elements = compareHash(elements);
        elements = multiplyInside(elements);
        elements = addDouble(elements);
        elements = multiplyParenthesis(elements);
        return finalProd(elements);
    }

    public static void init(Context context){
        String[] compare;
        masses = new HashMap<String,Double>();
        compare = null;
        try {
            InputStream is;
            is = context.getAssets().open("elements.csv");
            InputStreamReader r = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(r);

            compare = br.readLine().split(",");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < compare.length-1; i += 2) {
            masses.put(compare[i], Double.parseDouble(compare[i + 1]));
        }

    }
    //Replaces element abbreviations with molar masses
    public static ArrayList compareHash(ArrayList i){
        ArrayList modified = i;

        for(int k = 0; k < modified.size(); k++){
            if(modified.get(k) instanceof String){
                modified.set(k,masses.get(i.get(k)));
            }
        }
        return modified;
    }
    //Multiplies the number of element instances (ie. O3) with the molar mass
    public static ArrayList multiplyInside(ArrayList i){
        ArrayList modified = i;

        for(int k = modified.size()-1; k > 0; k--){
            if(modified.get(k) instanceof Integer && modified.get(k-1) instanceof Double){
                double f= Double.valueOf(modified.get(k).toString()).doubleValue();
                double s= Double.valueOf(modified.get(k-1).toString()).doubleValue();
                modified.set(k-1, f*s);
                modified.remove(k);
            }
        }
        return modified;
    }
    //Adds up the double values of molar mass
    public static ArrayList addDouble(ArrayList i){
        ArrayList modified = i;

        for(int k = modified.size()-1; k > 0; k--){
            if(modified.get(k) instanceof  Double && modified.get(k-1) instanceof  Double){
                double f = Double.valueOf(modified.get(k).toString()).doubleValue();
                double s = Double.valueOf(modified.get(k-1).toString()).doubleValue();
                modified.set(k-1, f+s);
                modified.remove(k);
            }
        }
        return modified;
    }
    //Multiplies totals inside parenthesis with subscript outside parenthesis ie. (NO3)3
    public static ArrayList multiplyParenthesis(ArrayList i){
        ArrayList modified = i;
        for(int k = modified.size()-1; k > 1; k--){
            if(modified.get(k) instanceof Integer && modified.get(k-1) instanceof  Character){
                double f = Double.valueOf(modified.get(k).toString()).doubleValue();
                double s = Double.valueOf(modified.get(k-2).toString()).doubleValue();
                modified.set(k-2, f*s);
                modified.remove(k);
            }

        }
        return modified;
    }
    //Returns MolarMass object with attribute "value" of type double molar mass
    public static MolarMass finalProd(ArrayList i){
        double finalMass;
        ArrayList modified = i;
        MolarMass finalMolarMass= new MolarMass();
        for(int k = modified.size()-1; k >= 0; k--){
            if(modified.get(k) instanceof Character) {
                modified.remove(k);
            }

        }
        if(modified.size() > 1) {
            for (int k = modified.size()-1; k > 0; k--) {
                double f = Double.valueOf(modified.get(k).toString()).doubleValue();
                System.out.println("MolarMass: "+modified.get(k-1));
                double s = Double.valueOf(modified.get(k-1).toString()).doubleValue();
                modified.set(k-1, f+s);
                modified.remove(k);
            }
        }
        finalMass= Double.valueOf(modified.get(0).toString()).doubleValue();
        finalMolarMass.setValue(finalMass);

        return finalMolarMass;
    }


    @Override
    public Unit getDefaultUnit() {
        return new FormulaUnit();
    }

    @Override
    public String getName() {
        return "Molar Mass";
    }

    @Override
    public Unit[] getUnits() {
        return new Unit[]{new FormulaUnit()};
    }

    public static class FormulaUnit extends Unit{

        @Override
        public String getName() {
            return "grams per mole";
        }

        @Override
        public String getShortName() {
            return "g/mol";
        }

        @Override
        public double toDefault(double inUnits) {
            return inUnits;
        }

        @Override
        public double fromDefault(double inDefault) {
            return inDefault;
        }
    }
}
