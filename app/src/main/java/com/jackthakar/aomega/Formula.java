package com.jackthakar.aomega;

import android.app.Fragment;

import com.jackthakar.aomega.gaslaws.ClausiusClapeyronFormula;
import com.jackthakar.aomega.gaslaws.IdealGasLawFormula;
import com.jackthakar.aomega.gaslaws.PartialPressuresFormula;
import com.jackthakar.aomega.stoich.LimitingReagentStoich;
import com.jackthakar.aomega.stoich.ReactionFormula;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by davidlee on 3/8/14.
 */
public abstract class Formula {
    public Datum[] variableList;
    public int answer = 0;

    public abstract int minVariables();
    public abstract int maxVariables();

    public abstract Fragment getLayout();

    public abstract String getFormulaName();

    public void setVariable(int index, Datum data){
        variableList[index]=data;
    }
    public abstract Datum[] getComponentList();
    public abstract Datum solveFor(int index);

    public static List<Formula> getFormulas(){
        List<Formula> formulas = new ArrayList<Formula>();
        formulas.add(new IdealGasLawFormula());
        formulas.add(new ReactionFormula());
        formulas.add(new ClausiusClapeyronFormula());
        formulas.add(new LimitingReagentStoich());
        formulas.add(new PartialPressuresFormula());
        return formulas;
    }
    public void reset(){variableList=null;}
}
