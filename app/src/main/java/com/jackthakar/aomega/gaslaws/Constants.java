package com.jackthakar.aomega.gaslaws;

/**
 * Created by davidlee on 3/8/14.
 */
public class Constants {
    public static final double GASCONSTANT_LITERSPASCALS=8314.5;
    public static final double GASCONSTANT_JOULES = 8.3145;
}
