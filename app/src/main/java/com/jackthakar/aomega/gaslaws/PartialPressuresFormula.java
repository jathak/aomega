package com.jackthakar.aomega.gaslaws;

import android.app.Fragment;

import com.jackthakar.aomega.Chemical;
import com.jackthakar.aomega.Datum;
import com.jackthakar.aomega.stoich.StoichFormula;

/**
 * Created by jathak on 3/12/14.
 */
public class PartialPressuresFormula extends StoichFormula {
    @Override
    public int minVariables() {
        return 0;
    }

    @Override
    public int maxVariables() {
        return getProductCount()+getReactantCount();
    }

    public int getReactantCount(){
        int i=0;
        for(Chemical c:equation.reactants){
            if(c.state == Chemical.GAS)i++;
        }
        return i;
    }
    public int getProductCount(){
        int i=0;
        for(Chemical c:equation.products){
            if(c.state == Chemical.GAS)i++;
        }
        return i;
    }

    @Override
    public Fragment getLayout() {
        return new IdealGasLawFragment();
    }

    @Override
    public String getFormulaName() {
        return "Dalton's Law of Partial Pressures";
    }

    @Override
    public Datum[] getComponentList() {
        Datum[] datums = new Datum[getProductCount()+getReactantCount()+1];
        if(variableList==null)variableList = new Datum[getProductCount()+getReactantCount()+1];
        int i=1;
        datums[0] = new Pressure().time(2);
        for(Chemical c:equation.reactants){
            if(c.state == Chemical.GAS){
                datums[i]=new Pressure().time(2).chemContext(c);
                i++;
            }
        }
        for(Chemical c:equation.products){
            if(c.state == Chemical.GAS){
                datums[i]=new Pressure().time(2).chemContext(c);
                i++;
            }
        }
        return datums;
    }

    @Override
    public Datum solveFor(int index) {
        int time = -2;
        int count = 0;
        boolean allReactants = true;
        boolean allProducts = true;
        for(int i=1;i<variableList.length;i++){
            Datum v = variableList[i];
            System.out.println("Dv: "+v);
            if(v!=null){
                time=v.time;
                count++;
                if(equation.reactants.contains(getComponentList()[i].chemContext)){
                    allProducts=false;
                }else if(equation.products.contains(getComponentList()[i].chemContext)){
                    allReactants=false;
                }
            }
        }
        System.out.println("time="+time+" count="+count);
        if(time==-2||count==0)return null;
        System.out.println("allReactants="+allReactants+" allProducts="+allProducts);
        if(allReactants){
            if(count<getReactantCount()&&variableList[0]==null)return null;
            if(variableList[0]==null){
                double totalPressure = 0;
                for(int i=1;i<=getReactantCount();i++){
                    totalPressure+=variableList[i].getValue();
                }
                variableList[0] = new Pressure().time(time);
                variableList[0].setValue(totalPressure);
                return variableList[0];
            }
            if(count<getReactantCount()-1)return null;
            double cPressure = variableList[0].getValue();
            int where = -1;
            for(int i=1;i<=getReactantCount();i++){
                if(variableList[i]!=null){
                    cPressure -= variableList[i].getValue();
                }else where=i;
            }
            variableList[where]=new Pressure().time(time).chemContext(getComponentList()[where].chemContext);
            variableList[where].setValue(cPressure);
            return variableList[where];
        }else if(allProducts){
            if(count<getProductCount()&&variableList[0]==null)return null;
            if(variableList[0]==null){
                double totalPressure = 0;
                for(int i=getProductCount();i<=variableList.length;i++){
                    totalPressure+=variableList[i].getValue();
                }
                variableList[0] = new Pressure().time(time);
                variableList[0].setValue(totalPressure);
                return variableList[0];
            }
            if(count<getProductCount()-1)return null;
            double cPressure = variableList[0].getValue();
            int where = -1;
            for(int i=getProductCount();i<=variableList.length;i++){
                if(variableList[i]!=null){
                    cPressure -= variableList[i].getValue();
                }else where=i;
            }
            variableList[where]=new Pressure().time(time).chemContext(getComponentList()[where].chemContext);
            variableList[where].setValue(cPressure);
            return variableList[where];
        }else{
            if(count<getReactantCount()+getProductCount()&&variableList[0]==null)return null;
            if(variableList[0]==null){
                double totalPressure = 0;
                for(int i=1;i<=variableList.length;i++){
                    totalPressure+=variableList[i].getValue();
                }
                variableList[0] = new Pressure().time(time);
                variableList[0].setValue(totalPressure);
                return variableList[0];
            }
            if(count<getReactantCount()+getProductCount()-1)return null;
            double cPressure = variableList[0].getValue();
            int where = -1;
            for(int i=1;i<=variableList.length;i++){
                if(variableList[i]!=null){
                    cPressure -= variableList[i].getValue();
                }else where=i;
            }
            variableList[where]=new Pressure().time(time).chemContext(getComponentList()[where].chemContext);
            variableList[where].setValue(cPressure);
            return variableList[where];
        }
    }
}
