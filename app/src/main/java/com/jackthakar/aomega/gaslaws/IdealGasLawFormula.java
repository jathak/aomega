package com.jackthakar.aomega.gaslaws;


import android.app.Fragment;

import com.jackthakar.aomega.Datum;
import com.jackthakar.aomega.Formula;
import com.jackthakar.aomega.stoich.Amount;

/**
 * Created by davidlee on 3/8/14.
 */
public class IdealGasLawFormula extends Formula {


    @Override
    public int minVariables() {
        return 3;
    }
    @Override
    public int maxVariables() {
        return 3;
    }

    @Override
    public Fragment getLayout() {
        return new IdealGasLawFragment();
    }

    @Override
    public String getFormulaName() {
        return "The Ideal Gas Law";
    }

    @Override
    //creates a new array for each variable that is to be used for ideal gas law formula
    public Datum[] getComponentList() {
        if(variableList==null)variableList=new Datum[4];
        return new Datum[]{new Pressure().time(2).chemOptional(true), new Volume().time(2),
                new Amount().time(2).chemOptional(true), new Temperature().time(2)};
    }

    @Override
    //solves for the variable that is not in variableList, but is in the componentlist
    public Datum solveFor(int index) {
        int count = 0;
        Datum unknown = null;

        for(int i=0; i<variableList.length; i++){
            if(variableList[i] == null){
                unknown = getComponentList()[i];
                count++;
            }
        }

        if(count!=1){
            //some error message
        }
        else{
            Datum pressure = variableList[0];
            Datum volume = variableList[1];
            Datum amount = variableList[2];
            Datum temperature = variableList[3];

            if (unknown.getName()=="Pressure"){
                return pressureSolver(volume, amount, temperature);
            }
            else if(unknown.getName()=="Volume"){
                return volumeSolver(pressure, amount, temperature);
            }
            else if(unknown.getName()=="Amount"){
                return amountSolver(pressure, volume, temperature);
            }
            else{
                return temperatureSolver(pressure, volume, amount);
            }
        }
        return null;
    }

    //methods to solve for specific pieces of information
    public Datum pressureSolver(Datum vDatum, Datum nDatum, Datum tDatum){
        double v = vDatum.getValue();
        double n = nDatum.getValue();
        double t = tDatum.getValue();
        double r = Constants.GASCONSTANT_LITERSPASCALS;

        Pressure pressure = new Pressure();
        pressure.setValue((n*r*t)/v);
        variableList[0] = pressure;
        if(nDatum.chemContext!=null)pressure.chemContext=nDatum.chemContext;
        return pressure.time(vDatum.time);
    }

    public Datum volumeSolver(Datum pDatum, Datum nDatum, Datum tDatum){
        double p = pDatum.getValue();
        double n = nDatum.getValue();
        double t = tDatum.getValue();
        double r = Constants.GASCONSTANT_LITERSPASCALS;

        Volume volume = new Volume();

        volume.setValue((n*r*t)/p);
        variableList[1] = volume;
        return volume.time(pDatum.time);
    }

    public Datum amountSolver(Datum pDatum, Datum vDatum, Datum tDatum){
        double p = pDatum.getValue();
        double v = vDatum.getValue();
        double t = tDatum.getValue();
        double r = Constants.GASCONSTANT_LITERSPASCALS;

        Amount amount = new Amount();
        amount.setValue((p*v)/(r*t));
        variableList[2] = amount;
        if(pDatum.chemContext!=null)amount.chemContext=pDatum.chemContext;
        return amount.time(vDatum.time);
    }

    public Datum temperatureSolver(Datum pDatum, Datum vDatum, Datum nDatum){
        double p = pDatum.getValue();
        double v = vDatum.getValue();
        double n = nDatum.getValue();
        double r = Constants.GASCONSTANT_LITERSPASCALS;

        Temperature temperature = new Temperature();
        temperature.setValue((p*v)/(r*n));

        variableList[3] = temperature;
        return temperature.time(vDatum.time);
    }
}
