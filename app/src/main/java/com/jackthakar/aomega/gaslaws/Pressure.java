package com.jackthakar.aomega.gaslaws;

import com.jackthakar.aomega.Datum;
import com.jackthakar.aomega.Unit;

/**
 * Created by jathak on 3/8/14.
 */
public class Pressure extends Datum {

    @Override
    public Unit getDefaultUnit() {
        return new Pascals();
    }

    @Override
    public String getName() {
        return "Pressure";
    }

    @Override
    public Unit[] getUnits() {
        return new Unit[]{new Pascals(),new Kilopascal(),new Atmospheres(),new MMHg()};
    }

    public static class Pascals extends Unit{
        @Override
        public String getName() {
            return "pascals";
        }

        @Override
        public String getShortName() {
            return "Pa";
        }

        @Override
        public double toDefault(double inUnits) {
            return inUnits;
        }

        @Override
        public double fromDefault(double inDefault) {
            return inDefault;
        }
    }
    public static class Atmospheres extends Unit{
        @Override
        public String getName() {
            return "atmospheres";
        }

        @Override
        public String getShortName() {
            return "atm";
        }

        @Override
        public double toDefault(double inUnits) {
            return inUnits*101325;
        }

        @Override
        public double fromDefault(double inDefault) {
            return inDefault/101325.0;
        }
    }
    public static class MMHg extends Unit{
        @Override
        public String getName() {
            return "millimeters of mercury";
        }

        @Override
        public String getShortName() {
            return "mmHg";
        }

        @Override
        public double toDefault(double inUnits) {
            return inUnits*133.3223684211;
        }

        @Override
        public double fromDefault(double inDefault) {
            return inDefault/133.3223684211;
        }
    }
    public static class Kilopascal extends Unit{
        @Override
        public String getName(){ return "kilopascals"; }

        @Override
        public String getShortName() { return "kPa"; }

        @Override
        public double toDefault(double inUnits) {return inUnits*1000; }

        @Override
        public double fromDefault(double inDefault) { return inDefault/1000.0;}
    }
}
