package com.jackthakar.aomega.gaslaws;

import com.jackthakar.aomega.Datum;
import com.jackthakar.aomega.Unit;

/**
 * Created by davidlee on 3/8/14.
 */
public class Temperature extends Datum{

    @Override
    public Unit getDefaultUnit(){ return new Kelvin(); }

    @Override
    public String getName() {
        return "Temperature";
    }

    @Override
    public Unit[] getUnits() { return new Unit[]{new Kelvin(), new Celsius(), new Fahrenheit()}; }

    public static class Kelvin extends Unit{
        @Override
        public String getName() { return "Kelvin"; }

        @Override
        public String getShortName() { return "K"; }

        @Override
        public double toDefault(double inUnits) { return inUnits; }

        @Override
        public double fromDefault(double inDefault) { return inDefault; }
    }

    public static class Celsius extends Unit{
        @Override
        public String getName() { return "degrees Celsius"; }

        @Override
        public String getShortName() { return "°C"; }

        @Override
        public double toDefault(double inUnits){
            return inUnits+273.15;
        }

        @Override
        public double fromDefault(double inDefault){
            return inDefault-273.15;
        }
    }

    public static class Fahrenheit extends Unit{
        @Override
        public String getName() { return "degrees Fahrenheit"; }

        @Override
        public String getShortName() { return "°F"; }

        @Override
        public double toDefault(double inUnits){
            return ((5.0/9.0)*(inUnits-32.0)+273.15);
        }

        @Override
        public double fromDefault(double inDefault){
            return ((inDefault-273.15)*(9.0/5.0) + 32.0);

        }
    }
}

