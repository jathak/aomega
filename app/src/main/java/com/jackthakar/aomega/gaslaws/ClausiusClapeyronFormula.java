package com.jackthakar.aomega.gaslaws;

import android.app.Fragment;

import com.jackthakar.aomega.Datum;
import com.jackthakar.aomega.Formula;

/**
 * Created by jathak on 3/9/14.
 */
public class ClausiusClapeyronFormula extends Formula {
    @Override
    public int minVariables() {
        return 4;
    }

    @Override
    public int maxVariables() {
        return 4;
    }

    @Override
    public Fragment getLayout() {
        return new ClausiusClapeyronFragment();
    }

    @Override
    public String getFormulaName() {
        return "The Clausius-Clapeyron Equation";
    }

    @Override
    public Datum[] getComponentList() {
        if(variableList==null)variableList = new Datum[5];
        return new Datum[]{new Pressure().time(-1),new Pressure().time(1),new Temperature().time(-1),new Temperature().time(1),new VaporizationEnthalpy().time(2)};
    }

    @Override
    public Datum solveFor(int index) {
        int counter = 0;
        int mIndex = index;
        for(int i=0;i<5;i++){
            if(variableList[i]==null){
                counter++;
                mIndex=i;
                System.out.println(mIndex);
            }
        }
        if(counter!=1)return null;
        Datum p1 = variableList[0];
        Datum p2 = variableList[1];
        Datum t1 = variableList[2];
        Datum t2 = variableList[3];
        Datum hv = variableList[4];
        if(mIndex==0){
            variableList[mIndex] = new Pressure().time(-1);
            double calc = p2.getValue()*Math.pow(Math.E,hv.getValue()/Constants.GASCONSTANT_JOULES*(1.0/t2.getValue()-1.0/t1.getValue()));
            variableList[mIndex].setValue(calc);
        }else if(mIndex==1){
            variableList[mIndex] = new Pressure().time(1);
            double calc = p1.getValue()*Math.pow(Math.E,hv.getValue()/Constants.GASCONSTANT_JOULES*(1.0/t1.getValue()-1.0/t2.getValue()));
            variableList[mIndex].setValue(calc);
        }else if(mIndex==4){
            variableList[mIndex] = new VaporizationEnthalpy().time(0);
            double calc = Math.log(p1.getValue()/p2.getValue())*Constants.GASCONSTANT_JOULES/((1/t2.getValue()-1/t1.getValue()));
            variableList[mIndex].setValue(calc);
        }else if(mIndex==2){
            variableList[mIndex] = new Temperature().time(-1);
            double calc = 1.0/(Math.log(p2.getValue()/p1.getValue())*Constants.GASCONSTANT_JOULES/hv.getValue()+1.0/t2.getValue());
            variableList[mIndex].setValue(calc);
        }else if(mIndex==3){
            variableList[mIndex] = new Temperature().time(1);
            double calc = 1.0/(Math.log(p1.getValue()/p2.getValue())*Constants.GASCONSTANT_JOULES/hv.getValue()+1.0/t1.getValue());
            variableList[mIndex].setValue(calc);
        }
        return variableList[mIndex];
    }
}
