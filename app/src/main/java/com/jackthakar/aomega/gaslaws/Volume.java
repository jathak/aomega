package com.jackthakar.aomega.gaslaws;

import com.jackthakar.aomega.Datum;
import com.jackthakar.aomega.Unit;

/**
 * Created by davidlee on 3/8/14.
 */
public class Volume extends Datum {

    @Override
    public Unit getDefaultUnit() { return new Liters(); }

    @Override
    public String getName() {
        return "Volume";
    }

    @Override
    public Unit[] getUnits() { return new Unit[]{new Liters(), new Milliliters()};}


    public static class Liters extends Unit{
        @Override
        public String getName(){
            return "Liters";
        }

        @Override
        public String getShortName(){
            return "L";
        }

        @Override public double toDefault(double inUnits){
            return inUnits;
        }

        @Override public double fromDefault(double inDefault){
            return inDefault;
        }
    }

    public static class Milliliters extends Unit{

        @Override
        public String getName(){
           return "Milliliters";
        }

        @Override
        public String getShortName(){
            return "mL";
        }

        @Override
        public double toDefault(double inUnits){
            return inUnits/1000;
        }

        @Override
        public double fromDefault(double inDefault){
            return inDefault*1000;
        }
    }
}
