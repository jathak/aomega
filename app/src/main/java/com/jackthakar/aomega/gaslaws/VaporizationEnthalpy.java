package com.jackthakar.aomega.gaslaws;

import com.jackthakar.aomega.Datum;
import com.jackthakar.aomega.Unit;

/**
 * Created by jathak on 3/9/14.
 */
public class VaporizationEnthalpy extends Datum {
    @Override
    public Unit getDefaultUnit() {
        return new MolarJoules();
    }

    @Override
    public String getName() {
        return "Enthalpy of Vaporization";
    }

    @Override
    public Unit[] getUnits() {
        return new Unit[]{new MolarJoules(),new MolarKilojoules()};
    }
    public static class MolarJoules extends Unit {

        @Override
        public String getName() {
            return "joules per mole";
        }

        @Override
        public String getShortName() {
            return "J/mol";
        }

        @Override
        public double toDefault(double inUnits) {
            return inUnits;
        }

        @Override
        public double fromDefault(double inDefault) {
            return inDefault;
        }
    }
    public static class MolarKilojoules extends Unit {

        @Override
        public String getName() {
            return "kilojoules per mole";
        }

        @Override
        public String getShortName() {
            return "kJ/mol";
        }

        @Override
        public double toDefault(double inUnits) {
            return inUnits*1000;
        }

        @Override
        public double fromDefault(double inDefault) {
            return inDefault/1000.0;
        }
    }
}
