package com.jackthakar.aomega;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jackthakar.android.wizardpager.model.AbstractWizardModel;
import com.jackthakar.android.wizardpager.model.BranchPage;
import com.jackthakar.android.wizardpager.model.ModelCallbacks;
import com.jackthakar.android.wizardpager.model.NumberPage;
import com.jackthakar.android.wizardpager.model.Page;
import com.jackthakar.android.wizardpager.model.SingleFixedChoicePage;
import com.jackthakar.android.wizardpager.ui.PageFragmentCallbacks;
import com.jackthakar.android.wizardpager.ui.ReviewFragment;
import com.jackthakar.android.wizardpager.ui.StepPagerStrip;
import com.jackthakar.aomega.gaslaws.Pressure;
import com.jackthakar.aomega.gaslaws.Temperature;
import com.jackthakar.aomega.gaslaws.VaporizationEnthalpy;
import com.jackthakar.aomega.gaslaws.Volume;
import com.jackthakar.aomega.stoich.Amount;
import com.jackthakar.aomega.stoich.Mass;

import java.util.ArrayList;
import java.util.List;

public class WizardActivity extends Activity implements
        PageFragmentCallbacks,
        ReviewFragment.Callbacks,
        ModelCallbacks {
    private ViewPager mPager;
    private MyPagerAdapter mPagerAdapter;

    private boolean mEditingAfterReview;

    private AbstractWizardModel mWizardModel;

    private boolean mConsumePageSelectedEvent;

    private Button mNextButton;
    private Button mPrevButton;

    private List<Page> mCurrentPageSequence;
    private StepPagerStrip mStepPagerStrip;
    public static Datum newDatum;

    public void onCreate(Bundle savedInstanceState) {
        mWizardModel = new DatumWizardModel(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard);

        if (savedInstanceState != null) {
            mWizardModel.load(savedInstanceState.getBundle("model"));
        }

        mWizardModel.registerListener(this);

        mPagerAdapter = new MyPagerAdapter(getFragmentManager());
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mPagerAdapter);
        mStepPagerStrip = (StepPagerStrip) findViewById(R.id.strip);
        mStepPagerStrip.setOnPageSelectedListener(new StepPagerStrip.OnPageSelectedListener() {
            @Override
            public void onPageStripSelected(int position) {
                position = Math.min(mPagerAdapter.getCount() - 1, position);
                if (mPager.getCurrentItem() != position) {
                    mPager.setCurrentItem(position);
                }
            }
        });

        mNextButton = (Button) findViewById(R.id.next_button);
        mPrevButton = (Button) findViewById(R.id.prev_button);

        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mStepPagerStrip.setCurrentPage(position);

                if (mConsumePageSelectedEvent) {
                    mConsumePageSelectedEvent = false;
                    return;
                }

                mEditingAfterReview = false;
                updateBottomBar();
            }
        });

        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPager.getCurrentItem() == mCurrentPageSequence.size()) {
                    finishSetup();
                } else {
                    if (mEditingAfterReview) {
                        mPager.setCurrentItem(mPagerAdapter.getCount() - 1);
                    } else {
                        mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                    }
                }
            }
        });

        mPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
            }
        });

        onPageTreeChanged();
        updateBottomBar();
    }

    @Override
    public void onResume(){
        super.onResume();
        if(mWizardModel==null)mWizardModel=new DatumWizardModel(this);
    }

    public void finishSetup(){
        BranchPage branchPage = ((BranchPage)mWizardModel.findByKey("Data Type"));
        String type = branchPage.getSelection();
        if(type.equals("Pressure")){
            newDatum = new Pressure();
        }else if(type.equals("Volume")){
            newDatum = new Volume();
        }else if(type.equals("Temperature")){
            newDatum = new Temperature();
        }else if(type.equals("Mass")){
            newDatum = new Mass();
        }else if(type.equals("Amount")){
            newDatum = new Amount();
        }else if(type.equals("Enthalpy of Vaporization")){
            newDatum = new VaporizationEnthalpy();
        }
        ArrayList<Page> subpages = new ArrayList<Page>();
        branchPage.flattenCurrentPageSequence(subpages);
        SingleFixedChoicePage unitPage = null;
        SingleFixedChoicePage chemPage = null;
        for(Page p:subpages){
            if(p.getTitle().equals("Units")){
                unitPage = (SingleFixedChoicePage)p;
            }
            if(p.getTitle().equals("Chemical")){
                chemPage = (SingleFixedChoicePage)p;
            }
        }
        String unitType = unitPage.getSelection();
        if(chemPage!=null){
            String selection = MainActivity.unsubize(chemPage.getSelection());
            if(!selection.equals("None"))newDatum.chemContext=new Chemical(selection);
        }
        //String unitType = ((SingleFixedChoicePage)mWizardModel.findByKey("Units")).getSelection();
        for(int i=0;i<newDatum.getUnits().length;i++){
            if(newDatum.getUnits()[i].getName().equals(unitType)){
                newDatum.currentUnit=i;
                break;
            }
        }
        String initfin = ((SingleFixedChoicePage)mWizardModel.findByKey("Initial or Final?")).getSelection();
        newDatum.time = 0;
        if(initfin.equals("Initial"))newDatum.time=Datum.INITIAL;
        if(initfin.equals("Final"))newDatum.time=Datum.FINAL;
        String value = ((NumberPage)mWizardModel.findByKey("Value")).getValue();
        double dValue = Double.parseDouble(value);
        newDatum.setValue(dValue,newDatum.getUnits()[newDatum.currentUnit]);
        finish();
    }

    @Override
    public void onPageTreeChanged() {
        mCurrentPageSequence = mWizardModel.getCurrentPageSequence();
        recalculateCutOffPage();
        mStepPagerStrip.setPageCount(mCurrentPageSequence.size() + 1); // + 1 = review step
        mPagerAdapter.notifyDataSetChanged();
        updateBottomBar();
    }

    private void updateBottomBar() {
        int position = mPager.getCurrentItem();
        if (position == mCurrentPageSequence.size()) {
            mNextButton.setText("Add Data");
            mNextButton.setBackgroundResource(R.drawable.finish_background);
            mNextButton.setTextAppearance(this, R.style.TextAppearanceFinish);
        } else {
            mNextButton.setText(mEditingAfterReview
                    ? R.string.review
                    : R.string.next);
            mNextButton.setBackgroundResource(R.drawable.selectable_item_background);
            TypedValue v = new TypedValue();
            getTheme().resolveAttribute(android.R.attr.textAppearanceMedium, v, true);
            mNextButton.setTextAppearance(this, v.resourceId);
            mNextButton.setEnabled(position != mPagerAdapter.getCutOffPage());
        }

        mPrevButton.setVisibility(position <= 0 ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWizardModel.unregisterListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle("model", mWizardModel.save());
    }

    @Override
    public AbstractWizardModel onGetModel() {
        return mWizardModel;
    }

    @Override
    public void onEditScreenAfterReview(String key) {
        for (int i = mCurrentPageSequence.size() - 1; i >= 0; i--) {
            if (mCurrentPageSequence.get(i).getKey().equals(key)) {
                mConsumePageSelectedEvent = true;
                mEditingAfterReview = true;
                mPager.setCurrentItem(i);
                updateBottomBar();
                break;
            }
        }
    }

    @Override
    public void onPageDataChanged(Page page) {
        if (page.isRequired()) {
            if (recalculateCutOffPage()) {
                mPagerAdapter.notifyDataSetChanged();
                updateBottomBar();
            }
        }
    }

    @Override
    public Page onGetPage(String key) {
        return mWizardModel.findByKey(key);
    }

    private boolean recalculateCutOffPage() {
        // Cut off the pager adapter at first required page that isn't completed
        int cutOffPage = mCurrentPageSequence.size() + 1;
        for (int i = 0; i < mCurrentPageSequence.size(); i++) {
            Page page = mCurrentPageSequence.get(i);
            if (page.isRequired() && !page.isCompleted()) {
                cutOffPage = i;
                break;
            }
        }

        if (mPagerAdapter.getCutOffPage() != cutOffPage) {
            mPagerAdapter.setCutOffPage(cutOffPage);
            return true;
        }

        return false;
    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter {
        private int mCutOffPage;
        private Fragment mPrimaryItem;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            if (i >= mCurrentPageSequence.size()) {
                return new ReviewFragment();
            }

            return mCurrentPageSequence.get(i).createFragment();
        }

        @Override
        public int getItemPosition(Object object) {
            // TODO: be smarter about this
            if (object == mPrimaryItem) {
                // Re-use the current fragment (its position never changes)
                return POSITION_UNCHANGED;
            }

            return POSITION_NONE;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            mPrimaryItem = (Fragment) object;
        }

        @Override
        public int getCount() {
            if (mCurrentPageSequence == null) {
                return 0;
            }
            return Math.min(mCutOffPage + 1, mCurrentPageSequence.size() + 1);
        }

        public void setCutOffPage(int cutOffPage) {
            if (cutOffPage < 0) {
                cutOffPage = Integer.MAX_VALUE;
            }
            mCutOffPage = cutOffPage;
        }

        public int getCutOffPage() {
            return mCutOffPage;
        }
    }
}