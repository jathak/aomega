package com.jackthakar.android.wizardpager.ui;

public interface HasPageFragmentCallbacks {
    void setPageFragmentCallbacks(PageFragmentCallbacks callbacks);
}
