/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jackthakar.android.wizardpager.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.Gravity;

public class DrawablePagerStrip extends BasePagerStrip {

    private Bitmap mPrevTabBitmap;
    private Bitmap mSelectedTabBitmap;
    private Bitmap mSelectedLastTabBitmap;
    private Bitmap mNextTabBitmap;

    private Rect mSourceRect = new Rect();
    private RectF mDestRectF = new RectF();
    private Paint paint = new Paint();

    public DrawablePagerStrip(Context context) {
        this(context, null, 0);
    }

    public DrawablePagerStrip(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DrawablePagerStrip(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        /*if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.DrawablePagerStrip, 0, 0);
            mPrevTabBitmap = BitmapFactory.decodeResource(context.getResources(), a.getResourceId(R.styleable.DrawablePagerStrip_previousTab, 0));
            mSelectedTabBitmap = BitmapFactory.decodeResource(context.getResources(), a.getResourceId(R.styleable.DrawablePagerStrip_selectedTab, 0));
            mSelectedLastTabBitmap = BitmapFactory.decodeResource(context.getResources(), a.getResourceId(R.styleable.DrawablePagerStrip_selectedLastTab, 0));
            mNextTabBitmap = BitmapFactory.decodeResource(context.getResources(), a.getResourceId(R.styleable.DrawablePagerStrip_nextTab, 0));

            mTabSpacing = a.getDimensionPixelSize(R.styleable.DrawablePagerStrip_tabSpacing, 0);
            a.recycle();
        }*/

        mTabWidth = Math.max(mPrevTabBitmap.getWidth(), mSelectedTabBitmap.getWidth());
        mTabWidth = Math.max(mTabWidth, mSelectedLastTabBitmap.getWidth());
        mTabWidth = Math.max(mTabWidth, mNextTabBitmap.getWidth());

        mTabHeight = Math.max(mPrevTabBitmap.getWidth(), mSelectedTabBitmap.getHeight());
        mTabHeight = Math.max(mTabHeight, mSelectedLastTabBitmap.getHeight());
        mTabHeight = Math.max(mTabHeight, mNextTabBitmap.getHeight());

        paint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mPageCount == 0) {
            return;
        }

        float totalWidth = mPageCount * (mTabWidth + mTabSpacing) - mTabSpacing;
        float totalLeft;
        boolean fillHorizontal = false;

        switch (mGravity & Gravity.HORIZONTAL_GRAVITY_MASK) {
            case Gravity.CENTER_HORIZONTAL:
                totalLeft = (getWidth() - totalWidth) / 2;
                break;
            case Gravity.RIGHT:
                totalLeft = getWidth() - getPaddingRight() - totalWidth;
                break;
            case Gravity.FILL_HORIZONTAL:
                totalLeft = getPaddingLeft();
                fillHorizontal = true;
                break;
            default:
                totalLeft = getPaddingLeft();
        }

        switch (mGravity & Gravity.VERTICAL_GRAVITY_MASK) {
            case Gravity.CENTER_VERTICAL:
                mDestRectF.top = (int) (getHeight() - mTabHeight) / 2;
                break;
            case Gravity.BOTTOM:
                mDestRectF.top = getHeight() - getPaddingBottom() - mTabHeight;
                break;
            default:
                mDestRectF.top = getPaddingTop();
        }

        mDestRectF.bottom = mDestRectF.top + mTabHeight;

        float tabWidth = mTabWidth;
        if (fillHorizontal) {
            tabWidth = (getWidth() - getPaddingRight() - getPaddingLeft()
                    - (mPageCount - 1) * mTabSpacing) / mPageCount;
        }

        for (int i = 0; i < mPageCount; i++) {
            Bitmap bitmap = i < mCurrentPage
                    ? mPrevTabBitmap
                    : (i > mCurrentPage
                    ? mNextTabBitmap
                    : (i == mPageCount - 1
                    ? mSelectedLastTabBitmap
                    : mSelectedTabBitmap));

            mSourceRect.set(0, 0, bitmap.getWidth(), bitmap.getHeight());

            mDestRectF.left = totalLeft + (i * (tabWidth + mTabSpacing));
            mDestRectF.right = mDestRectF.left + tabWidth;

            // Center image
            mDestRectF.top = (mTabHeight - bitmap.getHeight()) / 2;
            mDestRectF.bottom = mTabHeight - mDestRectF.top;
            mDestRectF.left += (mTabWidth - bitmap.getWidth()) / 2;
            mDestRectF.right -= (mTabWidth - bitmap.getWidth()) / 2;

            canvas.drawBitmap(bitmap, mSourceRect, mDestRectF, paint);
        }
    }
}
